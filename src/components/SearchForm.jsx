import { useGlobalContext } from "./context";

export const SearchForm = () => {
  const { setSearchTerm } = useGlobalContext();
  const submitHandler = (event) => {
    event.preventDefault();
    const searchValue = event.target.elements.search.value;
    if (!searchValue) {
      return;
    }
    setSearchTerm(searchValue);
  };
  return (
    <>
      <section>
        <h1 className="title">Image</h1>
        <form className="search-form" onSubmit={submitHandler}>
          <input
            type="text"
            className="form-input search-input"
            placeholder="Search Anything"
            name="search"
            id="search"
          />
          <button type="submit" className="btn">
            search
          </button>
        </form>
      </section>
    </>
  );
};

import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import { SearchForm } from "./components/SearchForm";
import { AppProvider } from "./components/context";
import { Gallary } from "./components/Gallary";

const queryClient = new QueryClient();
const App = () => {
  return (
    <>
      <AppProvider>
        <QueryClientProvider client={queryClient}>
          <SearchForm />
          <Gallary />
          <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
      </AppProvider>
    </>
  );
};

export default App;
